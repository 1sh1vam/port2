const nav_toggle = document.querySelector(".nav-toggle")
const arrow_down = document.querySelector(".more_link_down")
const arrow_up = document.querySelector(".more_link_up")
const hidden_projects = Array.from(document.querySelectorAll(".project-visible"))
const nav_links = Array.from(document.querySelectorAll(".nav_link"))

nav_toggle.addEventListener("click", (e)=>{
    document.querySelector(".header").classList.toggle("nav-open")
})

arrow_down.addEventListener("click", ()=>{
    hidden_projects.forEach(project => {
        project.classList.remove("hidden")
    })

    arrow_down.style.display = "none";
    arrow_up.style.display = "block";
})

nav_links.forEach(link => {
    link.addEventListener("click", ()=>{
        document.querySelector(".header").classList.remove("nav-open")
    })
})

arrow_up.addEventListener("click", () => {

    hidden_projects.forEach(project => {
        project.classList.add("hidden")
    })

    arrow_down.style.display = "block";
    arrow_up.style.display = "none";
})